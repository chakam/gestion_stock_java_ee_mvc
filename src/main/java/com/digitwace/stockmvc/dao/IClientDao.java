package com.digitwace.stockmvc.dao;

import com.digitwace.stockmvc.entities.Client;

public interface IClientDao extends IGenericDao<Client>{

}
