package com.digitwace.stockmvc.dao;

import com.digitwace.stockmvc.entities.Article;

public interface IArticleDao extends IGenericDao<Article>{

}
