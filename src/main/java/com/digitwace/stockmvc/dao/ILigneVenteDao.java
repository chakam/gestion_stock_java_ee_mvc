package com.digitwace.stockmvc.dao;

import com.digitwace.stockmvc.entities.LigneVente;

public interface ILigneVenteDao extends IGenericDao<LigneVente>{

}
