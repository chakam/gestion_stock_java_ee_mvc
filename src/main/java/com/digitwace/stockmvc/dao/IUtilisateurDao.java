package com.digitwace.stockmvc.dao;

import com.digitwace.stockmvc.entities.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur>{

}
