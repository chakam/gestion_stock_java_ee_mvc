package com.digitwace.stockmvc.dao;

import com.digitwace.stockmvc.entities.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurDao extends IGenericDao<LigneCommandeFournisseur> {

}
