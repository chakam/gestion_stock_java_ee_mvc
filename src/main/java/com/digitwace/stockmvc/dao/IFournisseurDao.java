package com.digitwace.stockmvc.dao;

import com.digitwace.stockmvc.entities.Fournisseur;

public interface IFournisseurDao extends IGenericDao<Fournisseur>{

}
