package com.digitwace.stockmvc.dao;

import java.io.InputStream;

public interface IFlickrDao {
	public String SavePhoto(InputStream stream, String fileName) throws Exception;

}
