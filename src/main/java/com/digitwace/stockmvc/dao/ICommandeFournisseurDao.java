package com.digitwace.stockmvc.dao;

import com.digitwace.stockmvc.entities.CommandeFournisseur;

public interface ICommandeFournisseurDao extends IGenericDao<CommandeFournisseur>{

}
