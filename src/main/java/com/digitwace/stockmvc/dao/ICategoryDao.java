package com.digitwace.stockmvc.dao;

import com.digitwace.stockmvc.entities.Category;

public interface ICategoryDao extends IGenericDao<Category>{

}
