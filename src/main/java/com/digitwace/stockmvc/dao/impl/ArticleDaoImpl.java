package com.digitwace.stockmvc.dao.impl;

import com.digitwace.stockmvc.dao.IArticleDao;
import com.digitwace.stockmvc.entities.Article;

public class ArticleDaoImpl extends GenericDaoImpl<Article> implements IArticleDao{

}
