package com.digitwace.stockmvc.dao.impl;

import java.io.InputStream;

import javax.swing.JOptionPane;

import com.digitwace.stockmvc.dao.IFlickrDao;
import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuth1Token;

import antlr.Token;



public class FlickrDaoImpl implements IFlickrDao{

	private Flickr flickr;
	
	private UploadMetaData uploadMetaData = new UploadMetaData();
	
	private String Apikey = "8778c826ecb09faf2b0e2388f9c4fc95";
	
	private String sharedSecret = "e66ce90de4b0e1d3";
	
	public void connect() {
		flickr = new Flickr(Apikey, sharedSecret, new REST());
		Auth auth = new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("");
		auth.setTokenSecret("");
		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
	}
	
	
	@Override
	public String SavePhoto(InputStream photo, String title)  throws Exception{
		connect();
		uploadMetaData.setTitle(title);
		String photoId = flickr.getUploader().upload(photo, uploadMetaData);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
	}
	
	public void auth() {
		flickr = new Flickr(Apikey, sharedSecret, new REST());
		
		AuthInterface authInterface = flickr.getAuthInterface();
		OAuth1RequestToken token = authInterface.getRequestToken();
		System.out.println("Token:" + token);
		
		String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
		System.out.println("Follow this url to autorize yourself on flickr");
		System.out.println(url);
		System.out.println("Paste the token in give you");
		
		String tokenKey = JOptionPane.showInputDialog(null);
		
		OAuth1Token requeToken = authInterface.getAccessToken(token, tokenKey);
		
		System.out.println("Authentification success");
		
		Auth auth = null;
		try {
			auth = authInterface.checkToken(requeToken);
		} catch (FlickrException e) {
			e.printStackTrace();
		}
		
		//This token can be used
		System.out.println("Token:" + requeToken.getToken());
		System.out.println("Secret:" + requeToken.getTokenSecret());
		System.out.println("Token:" + auth.getUser().getId());
		System.out.println("Token:" + auth.getUser().getRealName());
		System.out.println("Token:" + auth.getUser().getUsername());
		
		
	}

}
