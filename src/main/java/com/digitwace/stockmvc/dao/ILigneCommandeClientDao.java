package com.digitwace.stockmvc.dao;

import com.digitwace.stockmvc.entities.LigneCommandeClient;

public interface ILigneCommandeClientDao extends IGenericDao<LigneCommandeClient>{

}
