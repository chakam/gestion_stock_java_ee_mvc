package com.digitwace.stockmvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="articles")
public class Article implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="id_article")
	private Long idArticle;
	
	private String codeArticle;
	private String description;
	private BigDecimal prixUnitaireHT;
	private BigDecimal prixUnitaireTTC;
	private BigDecimal tauxTva;
	private String photo;
	
	@ManyToOne
	@JoinColumn(name="idCategory")
	private Category categorie;
	
	public Long getIdArticle() {
		return idArticle;
	}
	
	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}
	public String getCodeArticle() {
		return codeArticle;
	}
	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getPrixUnitaireHT() {
		return prixUnitaireHT;
	}
	public void setPrixUnitaireHT(BigDecimal prixUnitaireHT) {
		this.prixUnitaireHT = prixUnitaireHT;
	}
	public BigDecimal getPrixUnitaireTTC() {
		return prixUnitaireTTC;
	}
	public void setPrixUnitaireTTC(BigDecimal prixUnitaireTTC) {
		this.prixUnitaireTTC = prixUnitaireTTC;
	}
	public BigDecimal getTauxTva() {
		return tauxTva;
	}
	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public Category getCategorie() {
		return categorie;
	}
	public void setCategorie(Category categorie) {
		this.categorie = categorie;
	}
	public Article() {
		super();
	}
	
	

}
