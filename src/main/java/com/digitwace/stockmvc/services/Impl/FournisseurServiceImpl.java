package com.digitwace.stockmvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.digitwace.stockmvc.dao.IFournisseurDao;
import com.digitwace.stockmvc.dao.IFournisseurDao;
import com.digitwace.stockmvc.entities.Fournisseur;
import com.digitwace.stockmvc.services.IFournisseurService;
import com.digitwace.stockmvc.services.IFournisseurService;

//@Transactional
public class FournisseurServiceImpl implements IFournisseurService{

	private IFournisseurDao dao;
	
	public void setDao(IFournisseurDao dao) {
		 this.dao = dao;
	}
	@Override
	public Fournisseur save(Fournisseur entity) {
		return dao.save(entity);
	}

	@Override
	public Fournisseur update(Fournisseur entity) {
		return dao.update(entity);
	}

	@Override
	public List<Fournisseur> selectAll() {
		return dao.selectAll();
	}

	@Override
	public Fournisseur getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public List<Fournisseur> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Fournisseur findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Fournisseur findOne(String[] paramName, Object[] paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
