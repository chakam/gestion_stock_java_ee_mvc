package com.digitwace.stockmvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.digitwace.stockmvc.dao.ILigneVenteDao;
import com.digitwace.stockmvc.entities.LigneVente;
import com.digitwace.stockmvc.services.ILigneVenteService;

//@Transactional
public class LigneVenteServiceImpl implements ILigneVenteService{

	private ILigneVenteDao dao;
	
	public void setDao(ILigneVenteDao dao) {
		 this.dao = dao;
	}
	@Override
	public LigneVente save(LigneVente entity) {
		return dao.save(entity);
	}

	@Override
	public LigneVente update(LigneVente entity) {
		return dao.update(entity);
	}

	@Override
	public List<LigneVente> selectAll() {
		return dao.selectAll();
	}

	@Override
	public LigneVente getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public List<LigneVente> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneVente findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public LigneVente findOne(String[] paramName, Object[] paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
