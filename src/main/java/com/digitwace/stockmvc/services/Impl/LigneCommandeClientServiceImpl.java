package com.digitwace.stockmvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.digitwace.stockmvc.dao.ILigneCommandeClientDao;
import com.digitwace.stockmvc.entities.LigneCommandeClient;
import com.digitwace.stockmvc.services.ILigneCommandeClientService;

//@Transactional
public class LigneCommandeClientServiceImpl implements ILigneCommandeClientService{

	private ILigneCommandeClientDao dao;
	
	public void setDao(ILigneCommandeClientDao dao) {
		 this.dao = dao;
	}
	@Override
	public LigneCommandeClient save(LigneCommandeClient entity) {
		return dao.save(entity);
	}

	@Override
	public LigneCommandeClient update(LigneCommandeClient entity) {
		return dao.update(entity);
	}

	@Override
	public List<LigneCommandeClient> selectAll() {
		return dao.selectAll();
	}

	@Override
	public LigneCommandeClient getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public List<LigneCommandeClient> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneCommandeClient findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCommandeClient findOne(String[] paramName, Object[] paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
