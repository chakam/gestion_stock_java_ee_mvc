package com.digitwace.stockmvc.services;

import java.util.List;

import com.digitwace.stockmvc.entities.LigneCommandeClient;

public interface ILigneCommandeClientService {
	
	public LigneCommandeClient save (LigneCommandeClient entity);
	
	public LigneCommandeClient update(LigneCommandeClient entity);
	
	public List<LigneCommandeClient> selectAll();
	
	public LigneCommandeClient getById(Long id);
	
	public void remove(Long id);
	
	public List<LigneCommandeClient> selectAll(String sortField, String sort);
	
	public LigneCommandeClient findOne(String paramName, Object paramValue);
	
	public LigneCommandeClient findOne(String[] paramName, Object[] paramValue);
	
	public int findCountBy(String paramName, Object paramValue);

}
