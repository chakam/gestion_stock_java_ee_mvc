package com.digitwace.stockmvc.services;

import java.util.List;

import com.digitwace.stockmvc.entities.MvtStk;

public interface IMvtStkService {
	
	public MvtStk save (MvtStk entity);
	
	public MvtStk update(MvtStk entity);
	
	public List<MvtStk> selectAll();
	
	public MvtStk getById(Long id);
	
	public void remove(Long id);
	
	public List<MvtStk> selectAll(String sortField, String sort);
	
	public MvtStk findOne(String paramName, Object paramValue);
	
	public MvtStk findOne(String[] paramName, Object[] paramValue);
	
	public int findCountBy(String paramName, Object paramValue);

}
